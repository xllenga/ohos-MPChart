/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ImagePaint } from '@ohos/mpchart';
import { Fill } from '@ohos/mpchart';
import { XAxis, XAxisPosition } from '@ohos/mpchart'
import { YAxis, AxisDependency, YAxisLabelPosition } from '@ohos/mpchart'
import { WaterfallEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { WaterfallDataSet } from '@ohos/mpchart';
import { WaterfallData } from '@ohos/mpchart';
import { WaterfallChart, WaterfallChartModel } from '@ohos/mpchart'
import { IWaterfallDataSet } from '@ohos/mpchart'
import title from '../title/index';
import prompt from '@ohos.prompt';


@Entry
@Component
struct Index_waterfall_charts {
  @State model: WaterfallChartModel = new WaterfallChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  bottomAxis: XAxis = new XAxis();
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Toggle Bar Borders'];
  //标题栏标题
  private title: string = 'WaterfallChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()
  values: JArrayList<WaterfallEntry> | null = null;
  select: number = 0;
  @State maxValue: number = 160
  @State minValue: number = 60
  @State pointMinValue: number = 60
  @State pointMaxValue: number = 90
  bottomLableCount = 7;
  dialogController: CustomDialogController | null = new CustomDialogController({
    builder: WaterFallDialog({
      cancel: this.onCancel,
      confirm: () => {
        let minNum = 0;
        if(this.leftAxis){
          minNum = this.leftAxis!.getAxisMinimum();
        }
        this.values!.add(new WaterfallEntry(1, [this.minValue - minNum, this.maxValue - minNum], [this.pointMinValue - minNum, this.pointMaxValue - minNum]));
        this.bottomAxis.setLabelCount(++this.bottomLableCount, false);
        this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
        this.bottomAxis.setAxisMinimum(0);
        this.bottomAxis.setAxisMaximum(this.bottomLableCount);
        if(this.values!.size() > 10){
          this.model.setCustomCylinderWidth(0);
        }
        this.model.setXAxis(this.bottomAxis);
        this.model.xAixsMode.draw();
        this.model.invalidate();
      },
      maxValue: this.maxValue,
      minValue: this.minValue,
      pointMinValue: this.pointMinValue,
      pointMaxValue: this.pointMaxValue
    }),
    autoCancel: true,
    alignment: DialogAlignment.Bottom,
    offset: { dx: 0, dy: -20 },
    gridCount: 4,
    customStyle: false
  })

  // 在自定义组件即将析构销毁时将dialogControlle删除和置空
  aboutToDisappear() {
    // delete this.dialogController, // 删除dialogController
    if(this.dialogController){
      this.dialogController = null // 将dialogController置空
    }
  }

  onCancel() {
    console.info('Callback when the first button is clicked')
  }

  //标题栏菜单回调
  menuCallback() {
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if (index == undefined || index == -1) {
      return
    }
    switch (this.menuItemArr[index]) {
      case 'Toggle Bar Borders':
        for (let i = 0;i < this.model.getWaterfallData().getDataSets().length(); i++) {
          let barDataSet = this.model.getWaterfallData().getDataSets().get(i) as WaterfallDataSet;
          barDataSet.setBarBorderWidth(barDataSet.getBarBorderWidth() == 1 ? 0 : 1)
        }
        this.model.invalidate();
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear() {
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(40);
    this.leftAxis.setAxisMaximum(200);
    this.leftAxis.enableGridDashedLine(10, 10, 0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(40); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(200);

    this.bottomAxis.setLabelCount(this.bottomLableCount, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(7);

    this.initData(this.bottomAxis.getAxisMaximum(), this.leftAxis.getAxisMaximum())

    this.model.setWidth(this.mWidth);
    this.model.setHeight(this.mHeight);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();
    this.model.setCustomCylinderWidth(20);
    this.model.setRadius(20);
    this.model.setCylinderCenter(true)
  }

  build() {
    Column() {
      title({ model: this.titleModel })
      Stack({ alignContent: Alignment.TopStart }) {
        WaterfallChart({ model: this.model })
      }

      Row() {
        Button("删除")
          .margin(20)
          .onClick(() => {
            let range : string[] = []
            let size = this.values!.size()
            if (size <= 1) {
              prompt.showToast({
                message: '最后一个不能删除',
                duration: 1000,
              });
            } else {
              for (let i = 0; i < size; i++) {
                range.push("第" + (i + 1) + "列")
              }
              TextPickerDialog.show({
                range: range,
                selected: this.select,
                onAccept: (value: TextPickerResult) => {
                  // 设置select为按下确定按钮时候的选中项index，这样当弹窗再次弹出时显示选中的是上一次确定的选项
                  if(typeof (value.index) === "number"){
                    this.select = value.index
                    this.values!.removeIndex(value.index)
                  }
                  if(this.values!.size() < 10){
                    this.model.setCustomCylinderWidth(20);
                  }
                  this.modelSetData();
                  this.bottomAxis.setLabelCount(--this.bottomLableCount, false);
                  this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
                  this.bottomAxis.setAxisMinimum(0);
                  this.bottomAxis.setAxisMaximum(this.bottomLableCount);
                  this.model.setXAxis(this.bottomAxis);
                  this.model.xAixsMode.draw();
                  this.model.invalidate();
                },
              })
            }
          })
        Button("增加")
          .margin(20)
          .onClick(() => {
            if (this.dialogController != undefined) {
              this.dialogController.open()
            }
          })
      }
    }.alignItems(HorizontalAlign.Start)
  }

  private initData(count: number, range: number) {
    this.values = new JArrayList<WaterfallEntry>();
    let minNum = 0;
    if(this.leftAxis){
      minNum = this.leftAxis.getAxisMinimum()
    }
    this.values.add(new WaterfallEntry(1, [50 - minNum, 150 - minNum], [50 - minNum]));
    this.values.add(new WaterfallEntry(2, [80 - minNum, 120 - minNum], [120 - minNum]));
    this.values.add(new WaterfallEntry(3, [60 - minNum, 160 - minNum], [60 - minNum, 90 - minNum]));
    this.values.add(new WaterfallEntry(4, [90 - minNum, 160 - minNum], [95 - minNum]));
    this.values.add(new WaterfallEntry(5, [40 - minNum, 180 - minNum], [41 - minNum, 45 - minNum]));
    this.values.add(new WaterfallEntry(6, [100 - minNum, 200 - minNum], [100 - minNum, 130 - minNum]));
    this.values.add(new WaterfallEntry(7, [55 - minNum, 190 - minNum], [190 - minNum]));
    this.modelSetData();
  }

  private modelSetData() {
    let set1: WaterfallDataSet;

    if (this.model.getWaterfallData() != null &&
      this.model.getWaterfallData().getDataSetCount() > 0) {
      set1 = this.model.getWaterfallData().getDataSetByIndex(0) as WaterfallDataSet;
      set1.setValues(this.values!);
      this.model.getWaterfallData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new WaterfallDataSet(this.values!, "Statistics Vienna 2014");

      set1.setDrawIcons(false);
      set1.setColorsByVariable(this.getColors());
      set1.setDotsColor(0xe74c3c)
      set1.setStackLabels(["Births", "Divorces", "Marriages"]);
      set1.setValueTextSize(8)

      let dataSets: JArrayList<IWaterfallDataSet> = new JArrayList<IWaterfallDataSet>();
      dataSets.add(set1);

      let data: WaterfallData = new WaterfallData(dataSets);
      // data.setBarWidth(0);
      this.model.setData(data);
    }
  }

  private getColors(): number[] {

    let colors: number[] = [];

    colors.push(0x2ecc71)
    colors.push(0xf1c40f)

    return colors;
  }
}

@CustomDialog
struct WaterFallDialog {
  @Link maxValue: number
  @Link minValue: number
  @Link pointMinValue: number
  @Link pointMaxValue: number
  controller: CustomDialogController = new CustomDialogController({
    builder: WaterFallDialog({
      maxValue: this.maxValue,
      minValue: this.minValue,
      pointMinValue: this.pointMinValue,
      pointMaxValue: this.pointMaxValue
    }),
    autoCancel: true,
    alignment: DialogAlignment.Bottom,
    offset: { dx: 0, dy: -20 },
    gridCount: 4,
    customStyle: false
  })

  cancel: () => void = () => {};
  confirm: () => void = () => {};

  build() {
    Column() {
      Text('增加数据').fontSize(20).margin({ top: 10, bottom: 10 })
      Row() {
        Text("最大值").margin(5)
        TextInput({ placeholder: '最大值', text: this.maxValue + "" })
          .height(60)
          .width('70%')
          .type(InputType.Number)
          .onChange((value: string) => {
            this.maxValue = Number.parseInt(value)
          })
          .margin(10)
      }

      Row() {
        Text("最小值").margin(5)
        TextInput({ placeholder: '最小值', text: this.minValue + "" })
          .height(60)
          .width('70%')
          .type(InputType.Number)
          .onChange((value: string) => {
            this.minValue = Number.parseInt(value)
          })
          .margin(10)
      }

      Row() {
        Text("高亮最大值").margin(5)
        TextInput({ placeholder: '高亮最大值', text: this.pointMaxValue + "" })
          .height(60)
          .width('70%')
          .type(InputType.Number)
          .onChange((value: string) => {
            this.pointMaxValue = Number.parseInt(value)
          })
          .margin(10)
      }

      Row() {
        Text("高亮最小值").margin(5)
        TextInput({ placeholder: '高亮最小值', text: this.pointMinValue + "" })
          .height(60)
          .width('70%')
          .type(InputType.Number)
          .onChange((value: string) => {
            this.pointMinValue = Number.parseInt(value)
          })
          .margin(10)
      }

      Flex({ justifyContent: FlexAlign.SpaceAround }) {
        Button('取消')
          .onClick(() => {
            if(this.controller){
              this.controller.close()
            }
            this.cancel()
          }).backgroundColor(0xffffff).fontColor(Color.Black)
        Button('确定')
          .onClick(() => {
            if(this.controller){
              this.controller.close()
            }
            this.confirm()
          }).backgroundColor(0xffffff).fontColor(Color.Red)
      }.margin({ bottom: 10 })
    }
  }
}