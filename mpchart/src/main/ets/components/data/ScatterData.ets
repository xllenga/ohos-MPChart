/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import BarLineScatterCandleBubbleData from './BarLineScatterCandleBubbleData';
import IScatterDataSet from '../interfaces/datasets/IScatterDataSet';
import {JArrayList} from '../utils/JArrayList';

export default class ScatterData extends BarLineScatterCandleBubbleData<IScatterDataSet> {

    constructor(dataSets ?: JArrayList<IScatterDataSet>) {
        super(dataSets);
    }

    /**
     * Returns the maximum shape-size across all DataSets.
     *
     * @return
     */
    public getGreatestShapeSize() : number {

        let max : number = 0;
        if (this.mDataSets) {
            for (let data of this.mDataSets.dataSource) {
                let size : number = data.getScatterShapeSize();

                if (size > max)
                    max = size;
            }
        }
        return max;
    }
}
