/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import XAxisView from '../components/renderer/XAxisView';
import YAxisView, { YAxisModel }  from '../components/renderer/YAxisView';
import DefaultValueFormatter from '../formatter/DefaultValueFormatter';
import { XAxis } from '../components/XAxis';
import LegendRenderer from '../renderer/LegendRenderer';
import Description from '../components/Description';
import Paint, { LinePaint,TextPaint,PathPaint,RectPaint , ImagePaint}  from '../data/Paint';
import ViewPortHandler from '../utils/ViewPortHandler';
import Transformer from '../utils/Transformer';
import YAxis from '../components/YAxis';
import MyRect from '../data/Rect';
import BarEntry from '../data/BarEntry';
import BarChartRenderer from '../renderer/BarChartRenderer';
import BarData from '../data/BarData'
import {AxisDependency} from '../components/YAxis'
import Legend from '../components/Legend'
import Utils from '../utils/Utils'
import IBarDataSet from '../interfaces/datasets/IBarDataSet'
import BarDataSet from '../data/BarDataSet';
import { JArrayList } from '../utils/JArrayList';
import ScaleMode from '../data/ScaleMode';

@Component
export default struct BarChart {

  @ObjectLink model:BarChartModel
  visibilityAxis:Visibility=Visibility.Visible;

  public aboutToAppear() {
    this.model.invalidate()
  }

  build(){
    Stack({alignContent:Alignment.TopStart}) {
      Stack() {
        XAxisView({ scaleMode:this.model });
      }.visibility(this.visibilityAxis)
      .clip(new Path().commands(this.model.clipPath))
      Stack(){
        YAxisView({ model: this.model.leftAxisModel})
        YAxisView({ model: this.model.rightAxisModel})
      }.visibility(this.visibilityAxis)

      Stack({alignContent:Alignment.TopStart}){
        Stack(){
          if(this.model.paints != null && this.model.paints.length > 0){
            ForEach(this.model.paints, (item: Paint) => {
              if (item instanceof LinePaint) {
                Line()
                  .width(this.model.mWidth)
                  .height(this.model.mHeight)
                  .startPoint(item.startPoint)
                  .endPoint(item.endPoint)
                  .fill(item.fill)
                  .stroke(item.stroke)
                  .strokeWidth(item.strokeWidth)
                  .strokeDashArray(item.strokeDashArray)
                  .strokeDashOffset(item.strokeDashOffset)
                  .strokeOpacity(item.alpha)
                  .position({ x: 0, y: 0 })
                  .visibility(item.visibility)
              } else if (item instanceof TextPaint) {
                Text(item.text)
                  .position({ x: item.x, y: item.y })
                  .fontWeight(item.typeface)
                  .fontSize(item.textSize)
                  .textAlign(item.textAlign)
                  .fontColor(item.color)
                  .visibility(item.visibility)
              } else if (item instanceof PathPaint) {
                Path()
                  .commands(item.commands)
                  .fill(item.fill)
                  .stroke(item.stroke)
                  .strokeWidth(item.strokeWidth == 0 ? 1 : item.strokeWidth)
                  .strokeDashArray(item.strokeDashArray)
                  .strokeDashOffset(item.strokeDashOffset)
                  .strokeOpacity(item.alpha)
                  .position({ x: item.x, y: item.y })
                  .visibility(item.visibility)
              }else if(item instanceof RectPaint){
                if(item.linearGradientColors != undefined && item.linearGradientColors.length > 0){
                  Row()
                    .width(item.width)
                    .height(item.height)
                    .borderColor(item.stroke)
                    .borderWidth(item.strokeWidth + "px")
                    .position({x:item.x,y:item.y})
                    .linearGradient({direction: GradientDirection.Top,
                      colors:item.linearGradientColors})
                    .visibility(item.visibility)
                    .borderRadius({
                      topLeft: item.isShowBorder ? item.radius : 0,
                      topRight: item.isShowBorder ? item.radius : 0,
                    })
                }else{
                  Text()
                    .width(item.width)
                    .height(item.height)
                    .backgroundColor(item.fill)
                    .borderColor(item.stroke)
                    .borderWidth(item.strokeWidth +"px")
                    .position({x:item.x,y:item.y})
                    .visibility(item.visibility)
                    .fontSize(item.textSize)
                    .fontColor(item.color)
                    .borderRadius({
                      topLeft: item.isShowBorder ? item.radius : 0,
                      topRight: item.isShowBorder ? item.radius : 0,
                    })
                }
              }
            }, (item: Paint) => JSON.stringify(item))
          }

          ForEach(this.model.clickPaints, (item: Paint) => {
            if(item instanceof ImagePaint){
              Image($r('app.media.marker2'))
                .width(item.width)
                .height(item.height)
                .objectFit(ImageFit.Fill)
                .position({x:item.x,y:item.y})
                .visibility(item.visibility)
            } else if (item instanceof TextPaint) {
              Text(item.text)
                .width(item.width)
                .height(item.height)
                .position({ x: item.x, y: item.y })
                .fontWeight(item.typeface)
                .fontSize(item.textSize)
                .textAlign(item.textAlign)
                .fontColor(item.color)
                .visibility(item.visibility)
            } else if(item instanceof RectPaint){
              Text()
                .width(item.width)
                .height(item.height)
                .position({x:item.x,y:item.y})
                .linearGradient({direction: GradientDirection.Top,
                  colors:item.linearGradientColors})
                .visibility(item.visibility)
            }
          }, (item: Paint) => JSON.stringify(item))
        }
        .width(this.model.mViewPortHandler.contentWidth())
        .height(this.model.mViewPortHandler.contentHeight())
        .position({x:-this.model.paints[0].x + this.model.translateX
        ,y:-this.model.finalViewPortHandler.contentTop()+this.model.translateY})
      }
      .width(this.model.finalViewPortHandler.contentWidth() - this.model.paints[0].x)
      .height(this.model.finalViewPortHandler.contentHeight())
      .position({x:this.model.paints[0].x,y:this.model.finalViewPortHandler.contentTop()})
      .clip(new Rect({width:this.model.finalViewPortHandler.contentWidth() - this.model.paints[0].x,height:this.model.finalViewPortHandler.contentHeight()}))

    }
    .width(this.model.mViewPortHandler.getChartWidth())
    .height(this.model.mViewPortHandler.getChartHeight())
    .onClick((event: ClickEvent | undefined)=>{
      if (event) {
        this.model.onClick(event)
      }
    })
    .onTouch((event: TouchEvent | undefined)=>{
      if (event) {
        this.model.onTouch(event)
      }
    })
  }

}
@Observed
export class BarChartModel extends ScaleMode{
  /**
    * flag that indicates whether the highlight should be full-bar oriented, or single-value?
    */
  mHighlightFullBarEnabled:boolean = false;

  /**
    * if set to true, all values are drawn above their bars, instead of below their top
    */
  mDrawValueAboveBar:boolean = true;

  /**
     * if set to true, a grey area is drawn behind each bar that indicates the maximum value
     */
  mDrawBarShadow:boolean = false;

  mFitBars:boolean = false;
  mData:BarData = new BarData();
  mAxisLeft: YAxis = new YAxis();
  mAxisRight: YAxis = new YAxis();
  mXAxis: XAxis = new XAxis();
  mLeftAxisTransformer: Transformer | null = null;
  mRightAxisTransformer: Transformer | null = null;
  mMaxVisibleCount: number = 100;
  mViewPortHandler: ViewPortHandler = new ViewPortHandler();
  mRenderer: BarChartRenderer = new BarChartRenderer(this, this.mViewPortHandler);
  finalViewPortHandler: ViewPortHandler = new ViewPortHandler();
  minOffset: number = 15;
  mDescription: Description = new Description();
  mLegendRenderer: LegendRenderer | null = null;
  mLegend: Legend = new Legend();
  mDefaultValueFormatter: DefaultValueFormatter = new DefaultValueFormatter(0)
  paints:Paint[] = []
  private timerIdX: number = -1;
  private timerIdY: number = -1;
  private timerIdXY: number = -1;
  private curvesX: number = 0;
  private curvesY: number = 0;
  clickPaints:Paint[] = [];
  private mData2: BarData | null = null;
  mDrawClickText: boolean = false;
  translateX: number = 0;
  translateY: number = 0;
  translateCenterX: number = 0;
  translateCenterY: number = 0;
  isShowBorder: boolean = false

  clipPath: string = '';

  public init() {
    this.initViewPortHandler();
    this.mLeftAxisTransformer = new Transformer(this.mViewPortHandler);
    this.mRightAxisTransformer = new Transformer(this.mViewPortHandler);
    this.mRenderer = new BarChartRenderer(this, this.mViewPortHandler);
    this.mDescription = new Description();
  }

  public initViewPortHandler(){
    let leftMinOffset = this.mAxisLeft.isDrawLabelsEnabled()? this.minOffset : 0;
    let rightMinOffset = this.mAxisRight.isDrawLabelsEnabled()? this.minOffset : 0;
    this.mAxisLeft.getLongestLabel()
    this.mViewPortHandler.restrainViewPort(leftMinOffset, this.minOffset, rightMinOffset, this.minOffset)
    this.mViewPortHandler.setChartDimens(this.mWidth, this.mHeight);
    this.finalViewPortHandler.restrainViewPort(leftMinOffset, this.minOffset, rightMinOffset, this.minOffset)
    this.finalViewPortHandler.setChartDimens(this.mWidth,this.mHeight);
  }

  public resetViewPortHandler(){
    let leftMinOffset = this.mAxisLeft.isDrawLabelsEnabled()? this.minOffset : 0;
    let rightMinOffset = this.mAxisRight.isDrawLabelsEnabled()? this.minOffset : 0;
    this.mViewPortHandler.restrainViewPort(leftMinOffset, this.minOffset, rightMinOffset, this.minOffset)
    this.mViewPortHandler.setChartDimens(this.mWidth, this.mHeight);
  }

  public prepareMatrixValuePx(){
    if (this.mXAxis){
      if (this.mAxisRight && this.mRightAxisTransformer) {
        this.mRightAxisTransformer.prepareMatrixValuePx(this.mXAxis.mAxisMinimum,
          this.mXAxis.mAxisRange,
          this.mAxisRight.mAxisRange,
          this.mAxisRight.mAxisMinimum);
      }
      if (this.mLeftAxisTransformer && this.mAxisLeft ) {
        this.mLeftAxisTransformer.prepareMatrixValuePx(this.mXAxis.mAxisMinimum,
          this.mXAxis.mAxisRange,
          this.mAxisLeft.mAxisRange,
          this.mAxisLeft.mAxisMinimum);
      }
    }
  }

  protected calcMinMax() {

//    if (this.mFitBars) {
//      this.mXAxis.calculate(this.mData.getXMin() - this.mData.getBarWidth() / 2, this.mData.getXMax() + this.mData.getBarWidth() / 2);
//    } else {
//      this.mXAxis.calculate(this.mData.getXMin(), this.mData.getXMax());
//    }
//
//    // calculate axis range (min / max) according to provided data
//    this.mAxisLeft.calculate(this.mData.getYMin(AxisDependency.LEFT), this.mData.getYMax(AxisDependency.LEFT));
//    this.mAxisRight.calculate(this.mData.getYMin(AxisDependency.RIGHT), this.mData.getYMax(AxisDependency
//    .RIGHT));
  }

  /**
     * sets the number of maximum visible drawn values on the chart only active
     * when setDrawValues() is enabled
     *
     * @param count
     */
    public setMaxVisibleValueCount(count: number) {
        this.mMaxVisibleCount = count;
    }

    public getMaxVisibleCount(): number {
        return this.mMaxVisibleCount;
    }

    public setWidth(mWidth: number){
      this.mWidth = mWidth;
    }

    public setHeight(mHeight: number){
      this.mHeight = mHeight;
    }

  public setCustomCylinderWidth(cylinderWidth: number) {
    this.cylinderWidth = cylinderWidth;
  }

  public setCylinderCenter(cylinderCenter: boolean) {
    this.cylinderCenter = cylinderCenter;
  }

  public setTopRadius(radius: number) {
    this.radius = radius;
  }

  public setShowBorder(isShowBorder: boolean) {
    this.isShowBorder = isShowBorder;
  }

  public getShowBorder() {
    return this.isShowBorder
  }

  /**
     * Sets a new Description object for the chart.
     *
     * @param desc
     */
    public setDescription(desc: Description) {
        this.mDescription = desc;
    }

    /**
     * Returns the Description object of the chart that is responsible for holding all information related
     * to the description text that is displayed in the bottom right corner of the chart (by default).
     *
     * @return
     */
    public getDescription(): Description{
        return this.mDescription;
    }

  /**
    * Returns the y-axis object to the corresponding AxisDependency. In the
    * horizontal bar-chart, LEFT == top, RIGHT == BOTTOM
    *
    * @param axis
    * @return
    */
  public getAxis(axis:AxisDependency):YAxis | null {
    if (axis == AxisDependency.LEFT)
    return this.mAxisLeft;
    else
    return this.mAxisRight;
  }

  public isInverted(axis:AxisDependency):boolean {
    let axisResult = this.getAxis(axis);
    if (axisResult) {
      return axisResult.isInverted();
    }
   return false;
  }

  public setLeftYAxis(axis:YAxis){
    this.mAxisLeft = axis;
    this.leftAxisModel.setYAxis(this.mAxisLeft);
    this.leftAxisModel.setWidth(this.mWidth);
    this.leftAxisModel.setHeight(this.mHeight);
  }

  public setRightYAxis(axis:YAxis){
    this.mAxisRight = axis;
    this.rightAxisModel.setYAxis(this.mAxisRight);
    this.rightAxisModel.setWidth(this.mWidth);
    this.rightAxisModel.setHeight(this.mHeight);
    this.leftAxisModel.rightLongText = this.mAxisRight.isDrawLabelsEnabled()? this.mAxisLeft.getAxisMaximum() + "" : "";
    this.rightAxisModel.rightMinOffset = this.mAxisRight.isDrawLabelsEnabled()? this.minOffset : 0;
  }

  public setXAxis(axis:XAxis){
    this.mXAxis = axis;
    this.xAixsMode.topAxis.setDrawAxisLine(false)
    this.xAixsMode.bottomAxis=this.mXAxis
    this.xAixsMode.mWidth= this.mWidth
    this.xAixsMode.mHeight=this.mHeight
    this.xAixsMode.minOffset=this.minOffset
    this.xAixsMode.leftMinOffset = this.mAxisLeft.isDrawLabelsEnabled()? this.minOffset: 0;
    this.xAixsMode.rightMinOffset = this.mAxisRight.isDrawLabelsEnabled()? this.minOffset: 0;
    if (this.mAxisLeft) {
      this.xAixsMode.yLeftLongestLabel=this.mAxisLeft.getAxisMaximum() + ""
      this.xAixsMode.yRightLongestLabel=this.mAxisLeft.getAxisMaximum() + ""
    }
    this.calcXAixsModeClipPath();
    this.calcXLineClipPath();
  }

  public calcXAixsModeClipPath(){
    let rect = this.finalViewPortHandler.getContentRect();
    let textPaint: TextPaint = new TextPaint();

    let leftMinOffset = this.mAxisLeft.isDrawLabelsEnabled() ? this.minOffset : 0;
    let rightMinOffset = this.mAxisRight.isDrawLabelsEnabled() ? this.minOffset : 0;
    if (this.mAxisLeft) {
      textPaint.setTextSize(this.mAxisLeft.getTextSize())
      let maxTextOffset = Utils.calcTextWidth(textPaint, this.mAxisLeft.getAxisMinimum() < 0 ? this.mAxisLeft.getAxisMinimum()
        .toFixed(0) + "" : this.mAxisLeft.getAxisMaximum().toFixed(0) + "");
      ;
      this.clipPath = 'M' + Utils.convertDpToPixel(leftMinOffset) + ' ' + Utils.convertDpToPixel(0)
        + 'L' + Utils.convertDpToPixel(this.mWidth - rightMinOffset) + ' ' + Utils.convertDpToPixel(0)
        + 'L' + Utils.convertDpToPixel(this.mWidth - rightMinOffset) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + 'L' + Utils.convertDpToPixel(leftMinOffset) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + ' Z'
    }
  }

  public calcXLineClipPath(){
    let rect = this.finalViewPortHandler.getContentRect();
    this.xAixsMode.clipPath ='M'+Utils.convertDpToPixel(rect.left)+' '+Utils.convertDpToPixel(rect.top)
      +'L'+Utils.convertDpToPixel(rect.right)+' '+Utils.convertDpToPixel(rect.top)
      +'L'+Utils.convertDpToPixel(rect.right)+' '+Utils.convertDpToPixel(rect.bottom)
      +'L'+Utils.convertDpToPixel(rect.left)+' '+Utils.convertDpToPixel(rect.bottom)
      +' Z'
  }

  public getXAxis():XAxis | null {
    return this.mXAxis;
  }

  public getTransformer(which:AxisDependency):Transformer | null {
    if (which == AxisDependency.LEFT){
      return this.mLeftAxisTransformer;
    }else{
      return this.mRightAxisTransformer;
    }
  }

  public isDrawingValuesAllowed(): boolean{
    if (this.mData) {
      return this.mData.getEntryCount() < this.mMaxVisibleCount * this.mViewPortHandler.getScaleX();
    }
    return false;
  }

  /**
     * Returns the Highlight object (contains x-index and DataSet index) of the selected value at the given touch
     * point
     * inside the BarChart.
     *
     * @param x
     * @param y
     * @return
     */
  public getHighlightByTouchPoint(x:number, y:number)/*:Highlight*/ {

//    if (this.mData == null) {
//      console.error("Can't select by touch. No data set.")
//      return null;
//    } else {
//      let h:Highlight = this.getHighlighter().getHighlight(x, y);
//      if (h == null || !this.isHighlightFullBarEnabled()) return h;
//
//      // For isHighlightFullBarEnabled, remove stackIndex
//      return new Highlight(h.getX(), h.getY(),
//      h.getXPx(), h.getYPx(),
//      h.getDataSetIndex(), -1, h.getAxis());
//    }
  }

  /**
     * Returns the bounding box of the specified Entry in the specified DataSet. Returns null if the Entry could not be
     * found in the charts data.  Performance-intensive code should use void getBarBounds(BarEntry, RectF) instead.
     *
     * @param e
     * @return
     */
  public getBarBounds(e:BarEntry):MyRect {

    let bounds:MyRect = new MyRect();
    this.getBarBoundsRect(e, bounds);

    return bounds;
  }

  /**
     * The passed outputRect will be assigned the values of the bounding box of the specified Entry in the specified DataSet.
     * The rect will be assigned Float.MIN_VALUE in all locations if the Entry could not be found in the charts data.
     *
     * @param e
     * @return
     */
  public getBarBoundsRect( e:BarEntry,outputRect:MyRect) {

//    let bounds:MyRect = outputRect;
//
//    let dataSet:IBarDataSet = this.mData.getDataSetForEntry(e);
//
//    if (dataSet == null) {
//      this.bounds.set(Number.MIN_VALUE, Number.MIN_VALUE, Number.MIN_VALUE, Number.MIN_VALUE);
//      return;
//    }
//
//    let y:number = e.getY();
//    let x:number = e.getX();
//
//    let barWidth:number = this.mData.getBarWidth();
//
//    let left:number = x - barWidth / 2;
//    let right:number = x + barWidth / 2;
//    let top:number = y >= 0 ? y : 0;
//    let bottom:number = y <= 0 ? y : 0;
//
//    bounds.set(left, top, right, bottom);
//
//    this.getTransformer(dataSet.getAxisDependency()).rectValueToPixel(outputRect);
  }

  /**
     * If set to true, all values are drawn above their bars, instead of below their top.
     *
     * @param enabled
     */
  public setDrawValueAboveBar(enabled:boolean) {
    this.mDrawValueAboveBar = enabled;
  }

  /**
     * returns true if drawing values above bars is enabled, false if not
     *
     * @return
     */
  public isDrawValueAboveBarEnabled():boolean {
    return this.mDrawValueAboveBar;
  }

  /**
     * If set to true, a grey area is drawn behind each bar that indicates the maximum value. Enabling his will reduce
     * performance by about 50%.
     *
     * @param enabled
     */
  public setDrawBarShadow( enabled:boolean) {
    this.mDrawBarShadow = enabled;
  }

  /**
     * returns true if drawing shadows (maxvalue) for each bar is enabled, false if not
     *
     * @return
     */
  public isDrawBarShadowEnabled():boolean {
    return this.mDrawBarShadow;
  }

  /**
     * Set this to true to make the highlight operation full-bar oriented, false to make it highlight single values (relevant
     * only for stacked). If enabled, highlighting operations will highlight the whole bar, even if only a single stack entry
     * was tapped.
     * Default: false
     *
     * @param enabled
     */
  public setHighlightFullBarEnabled( enabled:boolean) {
    this.mHighlightFullBarEnabled = enabled;
  }

  /**
     * @return true the highlight operation is be full-bar oriented, false if single-value
     */
  public isHighlightFullBarEnabled():boolean {
    return this.mHighlightFullBarEnabled;
  }

  /**
     * Highlights the value at the given x-value in the given DataSet. Provide
     * -1 as the dataSetIndex to undo all highlighting.
     *
     * @param x
     * @param dataSetIndex
     * @param stackIndex   the index inside the stack - only relevant for stacked entries
     */
  public highlightValue( x:number, dataSetIndex:number,stackIndex:number) {
//    this.highlightValue(new Highlight(x, dataSetIndex, stackIndex), false);
  }

  public getBarData():BarData {
    return this.mData;
  }

  public setBarData(data:BarData){
    this.mData = data;
  }

  /**
     * Adds half of the bar width to each side of the x-axis range in order to allow the bars of the barchart to be
     * fully displayed.
     * Default: false
     *
     * @param enabled
     */
  public setFitBars( enabled:boolean) {
    this.mFitBars = enabled;
  }

  /**
     * Groups all BarDataSet objects this data object holds together by modifying the x-value of their entries.
     * Previously set x-values of entries will be overwritten. Leaves space between bars and groups as specified
     * by the parameters.
     * Calls notifyDataSetChanged() afterwards.
     *
     * @param fromX      the starting point on the x-axis where the grouping should begin
     * @param groupSpace the space between groups of bars in values (not pixels) e.g. 0.8f for bar width 1f
     * @param barSpace   the space between individual bars in values (not pixels) e.g. 0.1f for bar width 1f
     */
  public groupBars(fromX:number, groupSpace:number,barSpace:number) {
      let barData = this.getBarData();
    if (barData == null) {
      throw new Error("You need to set data for the chart before grouping bars.");
    } else {
      barData.groupBars(fromX, groupSpace, barSpace);
      this.notifyDataSetChanged();
    }
  }

  public notifyDataSetChanged() {

    if (this.mRenderer != null){
      this.mRenderer.initBuffers();
    }

    this.calcMinMax();

    //    this.mAxisRendererLeft.computeAxis(this.mAxisLeft.mAxisMinimum, this.mAxisLeft.mAxisMaximum, this.mAxisLeft.isInverted());
    //    this.mAxisRendererRight.computeAxis(this.mAxisRight.mAxisMinimum, this.mAxisRight.mAxisMaximum, this.mAxisRight.isInverted());
    //    this.mXAxisRenderer.computeAxis(this.mXAxis.mAxisMinimum, this.mXAxis.mAxisMaximum, false);

    if (this.mLegendRenderer != null){
      if (this.mData) {
        this.mLegendRenderer.computeLegend(this.mData);
      } else {
        this.mLegendRenderer.computeLegend();
      }
    }

    //    this.calculateOffsets();

  }

  public invalidate(){
    let tempPaints:Paint[]=[]
    if (this.mRenderer) {
      tempPaints = tempPaints.concat(this.mRenderer.drawData());
      tempPaints =tempPaints.concat(this.mRenderer.drawValues());
    }
    this.paints = [];
    this.paints=this.paints.concat(tempPaints)
  }

  public postInvalidate(){
    this.paints = [];
    setTimeout(()=>{
      if (this.mRenderer) {
        this.paints = this.paints.concat(this.mRenderer.drawData());
        this.paints = this.paints.concat(this.mRenderer.drawValues());
      }
    },50)
  }

  public setData(data: BarData) {

    this.mData = data;
    let dataSets2: JArrayList<IBarDataSet> = new JArrayList();
    let dataSetsArray = data.getDataSets();
    if (dataSetsArray) {
      for(let i = 0;i < dataSetsArray.length();i++){
        let dataSetObj = data.getDataSetByIndex(i);
        if (dataSetObj) {
          dataSets2.add(new BarDataSet(dataSetObj.getEntries() as JArrayList<BarEntry>,dataSetObj.getLabel()));
        }
      }
    }

    this.mData2 = new BarData(dataSets2);
    //        this.mOffsetsCalculated = false;

    if (data == null) {
      return;
    }

    // calculate how many digits are needed
    this.setupDefaultFormatter(data.getYMin(), data.getYMax());
    if (dataSetsArray) {
      for(let i = 0;i < dataSetsArray.length();i++){
        let dataSet = dataSetsArray.get(i);
        if (dataSet) {
          if (dataSet.needsFormatter() || dataSet.getValueFormatter() == this.mDefaultValueFormatter)
            dataSet.setValueFormatter(this.mDefaultValueFormatter);
        }
      }
    }

    // let the chart know there is new data
    this.notifyDataSetChanged();

  }

  /**
    * Calculates the required number of digits for the values that might be
    * drawn in the chart (if enabled), and creates the default-value-formatter
    */
  protected setupDefaultFormatter(min: number,max: number) {

    let reference: number = 0;

    if (this.mData == null || this.mData.getEntryCount() < 2) {

      reference = Math.max(Math.abs(min), Math.abs(max));
    } else {
      reference = Math.abs(max - min);
    }

    let digits: number = Utils.getDecimals(reference);

    // setup the formatter with a new number of digits
    this.mDefaultValueFormatter.setup(digits);
  }

  public animateX(durationMillis: number){
    clearInterval(this.timerIdX);
    clearInterval(this.timerIdY);
    this.curvesY = 0;
    this.curvesX = 0;
    this.timerIdX = setInterval(()=>{
      this.curvesX += 10;
      let count = this.paints.length;
      let isDrawValues = this.isDrawingValuesAllowed() && this.getBarData().getDataSetByIndex(0)?.isDrawValuesEnabled();
      if(isDrawValues){
        count = count / 2;
      }
      let position = Math.floor(this.curvesX / (durationMillis / count))
      for(let i = 0;i < count;i++){
        this.paints[i].setVisibility(position >= i?Visibility.Visible:Visibility.Hidden)
        if(isDrawValues){
          this.paints[i + this.paints.length / 2].setVisibility(position >= i?Visibility.Visible:Visibility.Hidden)
        }
      }
      if(this.clickPaints.length >= 1){
        this.clickPaints[0].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
      }
      if(this.clickPaints.length == 3){
        this.clickPaints[1].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
        this.clickPaints[2].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
      }
      if(this.curvesX >= durationMillis){
        this.curvesX = 0;
        clearInterval(this.timerIdX)
      }
    },40)
  }

  public animateY(durationMillis: number){
    clearInterval(this.timerIdX);
    clearInterval(this.timerIdY);
    let barData = this.getBarData();

    this.curvesY = 0;
    this.curvesX = 0;
    let clickPaints2:Paint[] = JSON.parse(JSON.stringify(this.clickPaints)) as Paint[]
    this.timerIdY = setInterval(()=>{
      if (!barData) {
        return;
      }
      this.curvesY += 0.01 / (durationMillis / 1000);
      for(let i = 0;i < barData.getDataSetCount();i++){
        if (!this.mData2) {
          break;
        }
        let dataSetByIndex:IBarDataSet | null = this.mData2.getDataSetByIndex(i);
        if (dataSetByIndex) {
          let entries = dataSetByIndex.getEntries();
          if (entries) {
            let values: JArrayList<BarEntry> = new JArrayList();
            for(let j = 0;j < entries.length();j++){
              let barEntry: BarEntry = entries.get(j) as BarEntry;
              let barEntryYValues = barEntry.getYVals();
              if(barEntry.isStacked() && barEntryYValues){
                let yVals: number[] = [];
                for(let j = 0;j < barEntryYValues.length;j++){
                  yVals[j] = barEntryYValues[j] * this.curvesY;
                }
                values.add(new BarEntry(barEntry.getX(),yVals))
              }else{
                values.add(new BarEntry(barEntry.getX(),barEntry.getY() * this.curvesY))
              }
            }
            let barDataByIndex = barData.getDataSetByIndex(i) as BarDataSet;
            if (barDataByIndex) {
              barDataByIndex.setValues(values);
            }
          }
        }
      }
      barData.notifyDataChanged();
      this.notifyDataSetChanged();
      this.invalidate();
      if(this.clickPaints.length >= 1){
        this.clickPaints[0].height = this.paints[this.clickPaints[0].clickPosition].height;
        this.clickPaints[0].y = this.paints[this.clickPaints[0].clickPosition].y;
      }
      if(this.clickPaints.length == 3){
        let imageOffsetY = clickPaints2[1].y - clickPaints2[0].y
        this.clickPaints[1].setY(this.clickPaints[0].getY() + imageOffsetY);
        this.clickPaints[2].setY(this.clickPaints[1].y + (Number(this.clickPaints[1].height) / 2) - Utils.calcTextHeight(this.clickPaints[2],(this.clickPaints[2] as TextPaint).text.replace(".","")))
      }

      if(this.curvesY >= 1){
        this.curvesY = 0;
        clearInterval(this.timerIdY)
      }
    },40)
  }

  public animateXY(durationMillisX: number,durationMillisY: number){
    clearInterval(this.timerIdX);
    clearInterval(this.timerIdY);
    clearInterval(this.timerIdXY);
    let barDataObj = this.getBarData();
    if (!barDataObj) {
      return;
    }
    this.curvesY = 0;
    this.curvesX = 0;
    let clickPaints2:Paint[] = JSON.parse(JSON.stringify(this.clickPaints)) as Paint[];
    this.timerIdXY = setInterval(()=>{
      if (!barDataObj) {
        return;
      }
      this.curvesY += 0.01 / (durationMillisY / 1000);
      this.curvesX += 10;
      let count = this.paints.length;
      let dataSetByIndex = barDataObj.getDataSetByIndex(0);
      for(let i = 0;i < barDataObj.getDataSetCount();i++){
        if (!this.mData2) {
          break;
        }

        let dataSetByIndex:IBarDataSet | null = this.mData2.getDataSetByIndex(i);
        if (dataSetByIndex) {
          let entries = dataSetByIndex.getEntries();
          if (entries) {
            let values: JArrayList<BarEntry> = new JArrayList();
            for(let j = 0;j < entries.length();j++){
              let barEntry: BarEntry | null = entries.get(j) as BarEntry;
              let barEntryYValues = barEntry.getYVals();
              if(barEntry.isStacked() && barEntryYValues){
                let yVals: number[] = [];
                for(let j = 0;j < barEntryYValues.length;j++){
                  yVals[j] = barEntryYValues[j] * this.curvesY;
                }
                values.add(new BarEntry(barEntry.getX(),yVals))
              }else{
                values.add(new BarEntry(barEntry.getX(),barEntry.getY() * this.curvesY))
              }
            }
            let barDataByIndex = barDataObj.getDataSetByIndex(i) as BarDataSet;
            if (barDataByIndex) {
              barDataByIndex.setValues(values)
            }
          }
        }
      }
      barDataObj.notifyDataChanged();
      this.notifyDataSetChanged();
      this.invalidate();
      if (dataSetByIndex) {
        let isDrawValues = this.isDrawingValuesAllowed() && dataSetByIndex.isDrawValuesEnabled();
        if(isDrawValues){
          count = count / 2;
        }
        let position = Math.floor(this.curvesX / (durationMillisX / count))
        for(let i = 0;i < count;i++){
          this.paints[i].setVisibility(position >= i?Visibility.Visible:Visibility.Hidden)
          if(isDrawValues){
            this.paints[i + this.paints.length / 2].setVisibility(position >= i?Visibility.Visible:Visibility.Hidden)
          }
        }
        if(this.clickPaints.length >= 1){
          this.clickPaints[0].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
        }
        if(this.clickPaints.length == 3){
          this.clickPaints[1].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
          this.clickPaints[2].visibility = position >= this.clickPaints[0].clickPosition?Visibility.Visible:Visibility.Hidden;
        }
        if(this.curvesX >= durationMillisX){
          this.curvesX = 0;
          clearInterval(this.timerIdXY);
        }
      }
      if(this.clickPaints.length >= 1){
        this.clickPaints[0].height = this.paints[this.clickPaints[0].clickPosition].height;
        this.clickPaints[0].y = this.paints[this.clickPaints[0].clickPosition].y;
      }
      if(this.clickPaints.length == 3){
        let imageOffsetY = clickPaints2[1].y - clickPaints2[0].y
        this.clickPaints[1].setY(this.clickPaints[0].getY() + imageOffsetY);
        this.clickPaints[2].setY(this.clickPaints[1].y + (Number(this.clickPaints[1].height) / 2) - Utils.calcTextHeight(this.clickPaints[2],(this.clickPaints[2] as TextPaint).text.replace(".","")))
      }

      if(this.curvesY >= 1){
        this.curvesY = 0;
        clearInterval(this.timerIdXY)
      }
    },40)
  }

  public onSingleClick(event: ClickEvent){
    let x = event.x;
    let y = event.y;
    let position = -1;
    for(let i = 0;i < this.paints.length;i++){
      if(this.paints[i] instanceof RectPaint){
        let rectPaint:RectPaint = this.paints[i] as RectPaint;
        if(x >= rectPaint.x + this.translateX && x <= (rectPaint.x + Number(rectPaint.width)+this.translateX)
        && y >= rectPaint.y + this.translateY && y <= (rectPaint.y + Number(rectPaint.height)+this.translateY)){
          position = i;
          this.click(position)
          break;
        }
      }
    }
  }

  public onDoubleClick(event?: ClickEvent){
    this.scale()
    if(this.clickPaints.length > 0){
      this.click(this.clickPaints[0].clickPosition)
    }
    this.translateCenterX = this.checkTranslateX(-(this.centerX * this.scaleX - this.centerX));
    this.translateCenterY = this.checkTranslateY(-(this.centerY * this.scaleY - this.centerY))
    this.translateX = this.translateCenterX;
    this.setXPosition(this.translateX)
  }

  public onMove(event: TouchEvent){
    let translateX = this.translateCenterX + this.moveX;
    let translateY = this.translateCenterY + this.moveY;

    this.translateX = this.checkTranslateX(translateX);
    this.moveX = this.translateX - this.translateCenterX
    this.translateY = this.checkTranslateY(translateY);
    this.moveY = this.translateY - this.translateCenterY
    this.leftAxisModel.translate(this.translateY);
    this.rightAxisModel.translate(this.translateY);
    this.setXPosition(this.translateX)
  }

  public onScale(event: TouchEvent){
    this.onDoubleClick()
  }

  private scale(){
    this.mWidth = this.finalViewPortHandler.getChartWidth() * this.scaleX;
    this.mHeight = this.finalViewPortHandler.getChartHeight() * this.scaleY;
    this.xAixsMode.mWidth = this.mWidth;
    this.resetViewPortHandler();
    this.invalidate();
  }

  private click(position:number){
    let textPaint :TextPaint|null = null;
    let paints2 = this.paints.concat([]);
    let rectPaint = this.paints[position];
    let barData = this.getBarData();
    if (barData){
      let dataSets = barData.getDataSets();
      if (dataSets) {
        let dataObj = dataSets.get(0);
        if (dataObj) {
          if(dataObj.isDrawValuesEnabled()){
            paints2.splice(0,paints2.length / 2);
            textPaint = paints2[position] as TextPaint;
            if (this.mRenderer) {
              this.clickPaints = this.mRenderer.drawClicked(rectPaint,textPaint,paints2.length,position);
            }
          }
        }
      }
    }
  }

  private getMaxWidth(): number{
    let count = this.paints.length;
    let barData = this.getBarData();
    if (barData){
      let dataSetByIndex = barData.getDataSetByIndex(0);
      if (dataSetByIndex) {
        let isDrawValues = this.isDrawingValuesAllowed() && dataSetByIndex.isDrawValuesEnabled();
        if(isDrawValues){
          count = count / 2;
        }
      }
    }
    return this.paints[count - 1].x + Number(this.paints[count - 1].width);
  }

  private checkTranslateX(translateX: number): number{
    if(translateX > 0){
      translateX = 0;
    }
    let maxWidth = this.getMaxWidth() - this.finalViewPortHandler.contentWidth();
    if(translateX < -maxWidth){
      translateX = -maxWidth;
    }
    return translateX;
  }

  private checkTranslateY(translateY: number): number{
    if(translateY < 0){
      translateY = 0;
    }
    let maxHeight = this.leftAxisModel.mHeight - this.finalViewPortHandler.getChartHeight()
    if(translateY > maxHeight){
      translateY = maxHeight;
    }
    return translateY;
  }

}
